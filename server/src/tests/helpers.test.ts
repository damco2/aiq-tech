import * as helpers from '../Controllers/helpers';

describe('helper tests', () => {
    test('check data type for sum of all', async () => {            // tests that the type of data for the sum is an Integer and not the error object.
        const sumAll = await helpers.getSumAll();
        expect(typeof sumAll).not.toBe('object');
    })
    
    test('check data type for sum of all states',async () => {
        const sumState = await helpers.getSumState('AZ');            // tests that the type of data for the sum for a particular state is an Integer and not the error object.
        expect(typeof sumState).not.toBe('object');
    })
})

