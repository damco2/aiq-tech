import axios from 'axios';

describe('Testing API\'s', () => {
    test('testing getStates', async () => {                                                                         // Tests the response code 
        const getStatesRes = await axios.get('http://localhost:5000/routes/getStates');
        expect(getStatesRes.status).toBe(200);
    })

    test('testing getTopNPlants', async () => {                                                                     // Tests the number of plants
        const getPlantsRes = await axios.get('http://localhost:5000/routes/getTopNPlants/5');
        expect(getPlantsRes.data.Content.length).toBe(5);
    })

    test('testing getTopNStatePlants', async () => {                                                                // Tests the number of plants for a state
        const getTopNStatePlantsRes = await axios.get('http://localhost:5000/routes/getTopNStatePlants/AK/10');
        expect(getTopNStatePlantsRes.data.Content.length).toBe(10);
        getTopNStatePlantsRes.data.Content.forEach((element: { Plant_state_abbreviation: any; }) => {
            expect(element.Plant_state_abbreviation).toBe('AK');
        });
    })
})