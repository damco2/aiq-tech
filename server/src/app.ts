import express, { Application, Request, Response, NextFunction } from "express";
import _ from 'lodash';
const routes = require('./Controllers/routes');
import cors from 'cors';

const app: Application = express();

app.use(cors({
  origin: '*'
}));

app.use('/routes', routes);

app.use("/", async (req: Request, res: Response, next: NextFunction) => {                               // default route
  res.set({
    'Content-Type': 'text/html'
  })
  res.send('<h2>Welcome to AIQ assigment.</h2>');
});


app.listen(5000, () => {
  console.log('Server is running on port 5000')
});
