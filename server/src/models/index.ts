import { Model, DataTypes } from "sequelize";
import db from "../config/database.config";

export class dataInstance extends Model {}

dataInstance.init(
  {
    eGRID2020_Plant_file_sequence_number: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      primaryKey: true,
      unique: true
    },
    Data_Year: {
      type: DataTypes.STRING(4),
      allowNull: false
    },
    Plant_state_abbreviation: {
      type: DataTypes.STRING(8),
      allowNull: false
    },
    Plant_name: {
      type: DataTypes.STRING(56),
      allowNull: false
    },
    DOEEIA_ORIS_plant_or_facility_code: {
      type: DataTypes.STRING(6),
      allowNull: false
    },
    Plant_transmission_or_distribution_system_owner_name: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    Plant_transmission_or_distribution_system_owner_ID: {
      type: DataTypes.STRING(7),
      allowNull: false
    },
    Utility_name: {
      type: DataTypes.STRING(61),
      allowNull: false
    },
    Utility_ID: {
      type: DataTypes.STRING(8),
      allowNull: false
    },
    Plantlevel_sector: {
      type: DataTypes.STRING(18),
      allowNull: true
    },
    Balancing_Authority_Name: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    Balancing_Authority_Code: {
      type: DataTypes.STRING(6),
      allowNull: false
    },
    NERC_region_acronym: {
      type: DataTypes.STRING(4),
      allowNull: false
    },
    eGRID_subregion_acronym: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    eGRID_subregion_name: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    Plant_associated_ISORTO_Territory: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_FIPS_state_code: {
      type: DataTypes.STRING(6),
      allowNull: false
    },
    Plant_FIPS_county_code: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    Plant_county_name: {
      type: DataTypes.STRING(21),
      allowNull: true
    },
    Plant_latitude: {
      type: DataTypes.STRING(9),
      allowNull: true
    },
    Plant_longitude: {
      type: DataTypes.STRING(11),
      allowNull: true
    },
    Number_of_units: {
      type: DataTypes.STRING(6),
      allowNull: false
    },
    Number_of_generators: {
      type: DataTypes.STRING(6),
      allowNull: false
    },
    Plant_primary_fuel: {
      type: DataTypes.STRING(7),
      allowNull: true
    },
    Plant_primary_fuel_category: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Flag_indicating_if_the_plant_burned_or_generated_any_amount_of_coal: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    Plant_capacity_factor: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_nameplate_capacity_MW: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    Nonbaseload_Factor: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    Biogas_biomass_plant_adjustment_flag: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    Combined_heat_and_power_CHP_plant_adjustment_flag: {
      type: DataTypes.STRING(7),
      allowNull: true
    },
    CHP_plant_useful_thermal_output_MMBtu: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    CHP_plant_power_to_heat_ratio: {
      type: DataTypes.STRING(7),
      allowNull: true
    },
    CHP_plant_electric_allocation_factor: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    Plant_pumped_storage_flag: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_annual_heat_input_from_combustion_MMBtu: {
      type: DataTypes.STRING(11),
      allowNull: true
    },
    Plant_ozone_season_heat_input_from_combustion_MMBtu: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Plant_total_annual_heat_input_MMBtu: {
      type: DataTypes.STRING(11),
      allowNull: true
    },
    Plant_total_ozone_season_heat_input_MMBtu: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Plant_annual_net_generation_MWh: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    Plant_ozone_season_net_generation_MWh: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Plant_annual_NOx_emissions_tons: {
      type: DataTypes.STRING(7),
      allowNull: true
    },
    Plant_ozone_season_NOx_emissions_tons: {
      type: DataTypes.STRING(7),
      allowNull: true
    },
    Plant_annual_SO2_emissions_tons: {
      type: DataTypes.STRING(7),
      allowNull: true
    },
    Plant_annual_CO2_emissions_tons: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Plant_annual_CH4_emissions_lbs: {
      type: DataTypes.STRING(9),
      allowNull: true
    },
    Plant_annual_N2O_emissions_lbs: {
      type: DataTypes.STRING(7),
      allowNull: true
    },
    Plant_annual_CO2_equivalent_emissions_tons: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Plant_annual_Hg_emissions_lbs: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_annual_NOx_total_output_emission_rate_lbMWh: {
      type: DataTypes.STRING(9),
      allowNull: true
    },
    Plant_ozone_season_NOx_total_output_emission_rate_lbMWh: {
      type: DataTypes.STRING(9),
      allowNull: true
    },
    Plant_annual_SO2_total_output_emission_rate_lbMWh: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    Plant_annual_CO2_total_output_emission_rate_lbMWh: {
      type: DataTypes.STRING(11),
      allowNull: true
    },
    Plant_annual_CH4_total_output_emission_rate_lbMWh: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    Plant_annual_N2O_total_output_emission_rate_lbMWh: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    Plant_annual_CO2_equivalent_total_output_emission_rate_lbMWh: {
      type: DataTypes.STRING(11),
      allowNull: true
    },
    Plant_annual_Hg_total_output_emission_rate_lbMWh: {
      type: DataTypes.STRING(7),
      allowNull: true
    },
    Plant_annual_NOx_input_emission_rate_lbMMBtu: {
      type: DataTypes.STRING(7),
      allowNull: true
    },
    Plant_ozone_season_NOx_input_emission_rate_lbMMBtu: {
      type: DataTypes.STRING(7),
      allowNull: true
    },
    Plant_annual_SO2_input_emission_rate_lbMMBtu: {
      type: DataTypes.STRING(7),
      allowNull: true
    },
    Plant_annual_CO2_input_emission_rate_lbMMBtu: {
      type: DataTypes.STRING(7),
      allowNull: true
    },
    Plant_annual_CH4_input_emission_rate_lbMMBtu: {
      type: DataTypes.STRING(7),
      allowNull: true
    },
    Plant_annual_N2O_input_emission_rate_lbMMBtu: {
      type: DataTypes.STRING(7),
      allowNull: true
    },
    Plant_annual_CO2_equivalent_input_emission_rate_lbMMBtu: {
      type: DataTypes.STRING(7),
      allowNull: true
    },
    Plant_annual_Hg_input_emission_rate_lbMMBtu: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_annual_NOx_combustion_output_emission_rate_lbMWh: {
      type: DataTypes.STRING(9),
      allowNull: true
    },
    Plant_ozone_season_NOx_combustion_output_emission_rate_lbMWh: {
      type: DataTypes.STRING(9),
      allowNull: true
    },
    Plant_annual_SO2_combustion_output_emission_rate_lbMWh: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    Plant_annual_CO2_combustion_output_emission_rate_lbMWh: {
      type: DataTypes.STRING(11),
      allowNull: true
    },
    Plant_annual_CH4_combustion_output_emission_rate_lbMWh: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    Plant_annual_N2O_combustion_output_emission_rate_lbMWh: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    Plant_annual_CO2_equivalent_combustion_output_emission_rate_lbMWh: {
      type: DataTypes.STRING(11),
      allowNull: true
    },
    Plant_annual_Hg_combustion_output_emission_rate_lbMWh: {
      type: DataTypes.STRING(7),
      allowNull: true
    },
    Plant_unadjusted_annual_NOx_emissions_tons: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_unadjusted_ozone_season_NOx_emissions_tons: {
      type: DataTypes.STRING(7),
      allowNull: true
    },
    Plant_unadjusted_annual_SO2_emissions_tons: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_unadjusted_annual_CO2_emissions_tons: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Plant_unadjusted_annual_CH4_emissions_lbs: {
      type: DataTypes.STRING(9),
      allowNull: true
    },
    Plant_unadjusted_annual_N2O_emissions_lbs: {
      type: DataTypes.STRING(7),
      allowNull: true
    },
    Plant_unadjusted_annual_Hg_emissions_lbs: {
      type: DataTypes.STRING(4),
      allowNull: true
    },
    Plant_unadjusted_annual_heat_input_from_combustion_MMBtu: {
      type: DataTypes.STRING(11),
      allowNull: true
    },
    Plant_unadjusted_ozone_season_heat_input_from_combustion_MMBtu: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Plant_unadjusted_total_annual_heat_input_MMBtu: {
      type: DataTypes.STRING(11),
      allowNull: true
    },
    Plant_unadjusted_total_ozone_season_heat_input_MMBtu: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Plant_unadjusted_annual_NOx_emissions_source: {
      type: DataTypes.STRING(13),
      allowNull: true
    },
    Plant_unadjusted_ozone_season_NOx_emissions_source: {
      type: DataTypes.STRING(13),
      allowNull: true
    },
    Plant_unadjusted_annual_SO2_emissions_source: {
      type: DataTypes.STRING(13),
      allowNull: true
    },
    Plant_unadjusted_annual_CO2_emissions_source: {
      type: DataTypes.STRING(13),
      allowNull: true
    },
    Plant_unadjusted_annual_CH4_emissions_source: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    Plant_unadjusted_annual_N2O_emissions_source: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    Plant_unadjusted_annual_Hg_emissions_source: {
      type: DataTypes.STRING(7),
      allowNull: false
    },
    Plant_unadjusted_annual_heat_input_source: {
      type: DataTypes.STRING(13),
      allowNull: true
    },
    Plant_unadjusted_ozone_season_heat_input_source: {
      type: DataTypes.STRING(13),
      allowNull: true
    },
    Plant_annual_NOx_biomass_emissions_tons: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_ozone_season_NOx_biomass_emissions_tons: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    Plant_annual_SO2_biomass_emissions_tons: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_annual_CO2_biomass_emissions_tons: {
      type: DataTypes.STRING(9),
      allowNull: true
    },
    Plant_annual_CH4_biomass_emissions_lbs: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_annual_N2O_biomass_emissions_lbs: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_combustion_heat_input_CHP_adjustment_value_MMBtu: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Plant_combustion_annual_ozone_season_heat_input_CHP_adjustment_value_MMBtu: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Plant_annual_NOx_emissions_CHP_adjustment_value_tons: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_ozone_season_NOx_emissions_CHP_adjustment_value_tons: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    Plant_annual_SO2_emissions_CHP_adjustment_value_tons: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_annual_CO2_emissions_CHP_adjustment_value_tons: {
      type: DataTypes.STRING(9),
      allowNull: true
    },
    Plant_annual_CH4_emissions_CHP_adjustment_value_lbs: {
      type: DataTypes.STRING(9),
      allowNull: true
    },
    Plant_annual_N2O_emissions_CHP_adjustment_value_lbs: {
      type: DataTypes.STRING(7),
      allowNull: true
    },
    Plant_nominal_heat_rate_BtukWh: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    Plant_annual_coal_net_generation_MWh: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Plant_annual_oil_net_generation_MWh: {
      type: DataTypes.STRING(9),
      allowNull: true
    },
    Plant_annual_gas_net_generation_MWh: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Plant_annual_nuclear_net_generation_MWh: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Plant_annual_hydro_net_generation_MWh: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Plant_annual_biomass_net_generation_MWh: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    Plant_annual_wind_net_generation_MWh: {
      type: DataTypes.STRING(9),
      allowNull: true
    },
    Plant_annual_solar_net_generation_MWh: {
      type: DataTypes.STRING(9),
      allowNull: true
    },
    Plant_annual_geothermal_net_generation_MWh: {
      type: DataTypes.STRING(9),
      allowNull: true
    },
    Plant_annual_other_fossil_net_generation_MWh: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    Plant_annual_other_unknown_purchased_fuel_net_generation_MWh: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    Plant_annual_total_nonrenewables_net_generation_MWh: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Plant_annual_total_renewables_net_generation_MWh: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Plant_annual_total_nonhydro_renewables_net_generation_MWh: {
      type: DataTypes.STRING(9),
      allowNull: true
    },
    Plant_annual_total_combustion_net_generation_MWh: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Plant_annual_total_noncombustion_net_generation_MWh: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Plant_coal_generation_percent_resource_mix: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_oil_generation_percent_resource_mix: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_gas_generation_percent_resource_mix: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_nuclear_generation_percent_resource_mix: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_hydro_generation_percent_resource_mix: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_biomass_generation_percent_resource_mix: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_wind_generation_percent_resource_mix: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_solar_generation_percent_resource_mix: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_geothermal_generation_percent_resource_mix: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_other_fossil_generation_percent_resource_mix: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_other_unknown_purchased_fuel_generation_percent_resource_mix: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_total_nonrenewables_generation_percent_resource_mix: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_total_renewables_generation_percent_resource_mix: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_total_nonhydro_renewables_generation_percent_resource_mix: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_total_combustion_generation_percent_resource_mix: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    Plant_total_noncombustion_generation_percent_resource_mix: {
      type: DataTypes.STRING(6),
      allowNull: true
    }
  },
  {
    sequelize: db,
    tableName: "aiq_data",
    timestamps: false
  }
);
