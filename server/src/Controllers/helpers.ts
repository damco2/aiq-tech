import { dataInstance } from '../models/index';
import { Op } from "sequelize";

export async function getSumAll() {                                   // return's the sum of all plants for the % calculation
    let total_sum = 0;
    try {
      let data = await dataInstance.findAll({
        attributes: ["Plant_annual_net_generation_MWh"],
        where: {
          Plant_annual_net_generation_MWh: {
            [Op.ne]: null,
            [Op.ne]: "PLNGENAN",
          },
        },
      });
      const dataArr = JSON.parse(JSON.stringify(data));
      total_sum = dataArr.reduce((sum: number, currEle: any) => {
        if (!isNaN(parseFloat(currEle.Plant_annual_net_generation_MWh))) {
          return (sum += parseFloat(currEle.Plant_annual_net_generation_MWh));
        }
        return sum;
      }, 0);
  
      return total_sum;
    } catch (e) {
      console.log("Not able to generate the sum");
      return new Error();
    }
  }
  
 export async function getSumState(state: string) {                   // return's the sum of all plants from a particular state for the % calculation.
    let total_sum = 0;
    try {
      let data = await dataInstance.findAll({
        attributes: ["Plant_annual_net_generation_MWh"],
        where: {
          Plant_state_abbreviation: {
            [Op.eq]: state,
          },
        },
      });
      const dataArr = JSON.parse(JSON.stringify(data));
      total_sum = dataArr.reduce((sum: number, currEle: any) => {
        if (!isNaN(parseFloat(currEle.Plant_annual_net_generation_MWh))) {
          return (sum += parseFloat(currEle.Plant_annual_net_generation_MWh));
        }
        return sum;
      }, 0);
      return total_sum;
    } catch (e) {
      console.log("Not able to generate the sum");
      return new Error();
    }
  }