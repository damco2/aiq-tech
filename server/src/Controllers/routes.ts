import express, { Request, Response, NextFunction } from "express";
import db from "../config/database.config";
import { dataInstance } from "../models";
import _ from "lodash";
import { Op } from "sequelize";
import * as helpers from './helpers';

const router = express.Router();

db.sync().then(() => {
  console.log("Connected to the database.");
});

router.get(                                                         // get's all state name abbrevations and returns it in response
  "/getStates",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      let data = await dataInstance.findAll({
        attributes: ["Plant_state_abbreviation"],
        where: {
          Plant_state_abbreviation: {
            [Op.ne]: "PSTATABB",
          },
        },
      });
      data = _.uniqBy(data, "Plant_state_abbreviation");
      res.json(data);
    } catch (e) {
      res.status(400).send("Unable to fetch data.");
    }
  }
);

router.get(
  "/getTopNPlants/:limit",                                            // get's top N plants
  async (req: Request, res: Response, next: NextFunction) => {
    const limit = req.params.limit;
    try {
      let data = await dataInstance.findAll({
        order: [["Plant_annual_net_generation_MWh", "DESC"]],
        where: {
          Plant_annual_net_generation_MWh: {
            [Op.ne]: null,
            [Op.and]: [{[Op.ne]: ""}, {[Op.ne]: "PLNGENAN"}],
          },
        },
        limit: parseInt(limit),
      });
      let sum = await helpers.getSumAll();
      if(typeof sum === 'object'){
        res.status(400).send("Unable to generate the sum.");
        return;
      }
      res.json({Content: data, total_sum: sum});
    } catch (e) {
      res.status(400).send("Unable to fetch data.");
    }
  }
);

router.get(
  "/getTopNStatePlants/:state/:limit",                              // get's top N plants from a particular state
  async (req: Request, res: Response, next: NextFunction) => {
    const state = req.params.state;
    const limit = req.params.limit;
    try {
      let data = await dataInstance.findAll({
        order: [["Plant_annual_net_generation_MWh", "DESC"]],
        where: {
          Plant_state_abbreviation: {
            [Op.eq]: state
          },
          Plant_annual_net_generation_MWh: {
            [Op.ne]: null,
            [Op.and]: [{[Op.ne]: ""}, {[Op.ne]: "PLNGENAN"}],
          }
        },
        limit: parseInt(limit)
      });
      let sum = await helpers.getSumState(state);
      if(typeof sum === 'object'){
        res.status(400).send("Unable to generate the sum.");
        return;
      }
      res.json({Content: data, total_sum: sum});
    } catch (e) {
      res.status(400).send(e);
    }
  }
);

module.exports = router;
