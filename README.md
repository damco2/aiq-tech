# Backend step to run the project
1.	Run npm install to install packages
2.	Use npm run dev to start the server in development mode (pure typescript).
3.	The server by default runs on port 5000.
4.	For running the test cases use npm test.

Important: Only run the test cases in the dev environment and always run the test cases after starting the server.
OR
1.	For production build run npm run build, this will create a production dist folder. This dist folder will contain server files in .js which is compiled from the .ts files from dev.

Please Note: The previous step has been performed in order to enhance the performance in production mode as every time the server runs for NodeJS in any way the typescript is compiled to JavaScript internally for the V8 engine, as the V8 engine cannot process Typescript.

2.	Rename the existing src folder as src_dev_ts and the new dist folder as src.
3.	Use npm start to start the server in production mode.
Please Note: The database which is used in this implementation is sqlite as the data is very tabular and development and prod runtime efficiency can be achieved in this scenario using SQL database by making use of queries.

# Endpoints for live demo:
1.	demo13.damcogroup.com:3009/routes/getStates		// gives a list of all the states
2.	demo13.damcogroup.com:3009/routes/getTopNPlants/N	// gives a list of top N plants
3.	demo13.damcogroup.com:3009/routes/getTopNStatePlants/stateNameAbbrevation/N // gives a list of top N plants from a particular state	

# Frontend step to run the project
1.	Run npm install to install packages
2.	Use npm start to start the server.
3.	The server by default runs on port 4200.
4.	For running the test cases use npm test.

# Tool used
1. NodeJS, Angular, TypeScript, Bootstrap
2. Database - SQLite
3. Used Google Maps to show plants

# Demo Url
http://demo13.damcogroup.com:27017/application1
