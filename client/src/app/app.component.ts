import { Component } from '@angular/core';
import { ApiService } from './services/api.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  states: any;
  selectedState = 'all';
  plantCounts = [5, 10, 15, 20, 25, 30];
  selectedNplant = 5;
  plants: any;
  constructor(private apiService: ApiService) { 
  }

  ngOnInit() {
    this.getStates();
    this.getTopNPlants();
  }
  
  /**
   * get states
   * @private
   */
  private getStates() {
    this.apiService.getStates().subscribe({next: (response: any) => {
      this.states = response;
      }, error: (error) => {
        console.error(error);
      }
    });
  }

  /**
   * get top n plants
   * @private
   */
  private getTopNPlants() {
    this.apiService.getTopNPlants(this.selectedNplant).subscribe({next: (response: any) => {
      this.plants = response;
      }, error: (error) => {
        console.error(error);
      }
    });
  }
  
  /**
   * get top n state plants
   * @private
   */
  private getTopNStatePlants(): void {
    this.apiService.getTopNStatePlants(this.selectedNplant, this.selectedState).subscribe({next: (response: any) => {
      this.plants = response;
      }, error: (error) => {
        console.error(error);
      }
    });
  }

  /**
   * state change method
   * @public
   */
  public stateChange() {
    if (this.selectedState === 'all') {
      this.getTopNPlants();
    } else {
      this.getTopNStatePlants();
    }
  }

}
