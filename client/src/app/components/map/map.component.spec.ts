import { HttpClientModule } from '@angular/common/http';
import { SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapComponent } from './map.component';

describe('MapComponent', () => {
  let component: MapComponent;
  let fixture: ComponentFixture<MapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientModule],
      declarations: [ MapComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be greater than', () => {
    component.plants = {Content: [{lat: '10'}]};
    component.ngOnChanges({
      plants:  new SimpleChange(null, component.plants, false) 
    });
    fixture.detectChanges();
    expect(component.markerPositions.length).toBeGreaterThan(0);
  });
});
