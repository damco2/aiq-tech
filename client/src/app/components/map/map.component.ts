import { Component, OnInit, Input, OnChanges, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map, Observable, of } from 'rxjs';
import { ApiService } from '../../services/api.service';
import { MapInfoWindow, MapMarker } from '@angular/google-maps';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit, OnChanges {
  apiLoaded: Observable<boolean>;
  center: google.maps.LatLngLiteral = {lat: 37.090240, lng: -95.712891};
  zoom = 4;
  markerOptions: google.maps.MarkerOptions = {draggable: false};
  markerPositions: google.maps.LatLngLiteral[] = []; 
  infoWindowContent: any;
  @Input() plants: any;
  @Input() selectedState!: string;
  @ViewChild(MapInfoWindow) infoWindow!: MapInfoWindow;

  constructor(private httpClient: HttpClient, private apiService: ApiService) {
    this.infoWindowContent = {};
    this.apiLoaded = httpClient.jsonp(`https://maps.googleapis.com/maps/api/js?key=${environment.googleMapKey}`, 'callback')
      .pipe(
        map(() => true),
        catchError(() => of(false)),
      );
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: any): void {
    if ('plants' in changes && this.plants) {
      this.setMarkers();
    }
  }

  /**
   * set google map markers
   * @private
   */
  private setMarkers(): void {
    this.markerPositions = [];
    this.plants.Content.forEach((row: any) => {
      this.markerPositions.push({lat: Number(row.Plant_latitude), lng: Number(row.Plant_longitude)});
    });
    
    if (this.selectedState === 'all') {
      this.center = {lat: 37.090240, lng: -95.712891};
    } else {
      this.center = this.markerPositions[0];
    }
  }
  
  /**
   * open marker info popup
   * @param {MapMarker} marker : Marker dom element
   * @param {number} i : Marker index  
   * @public
   */
  openInfoWindow(marker: MapMarker, i: number): void {
    const row = this.plants.Content[i];
    this.infoWindowContent = {
      plantName: row.Plant_name,
      absoluteValue: row.Plant_annual_net_generation_MWh,
      percentage: (row.Plant_annual_net_generation_MWh) ? ((parseFloat(row.Plant_annual_net_generation_MWh) / this.plants.total_sum) * 100).toFixed(4) : 0
    }
    this.infoWindow.open(marker);
  }

}
