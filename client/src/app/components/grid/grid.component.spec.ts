import { SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridComponent } from './grid.component';

describe('GridComponent', () => {
  let component: GridComponent;
  let fixture: ComponentFixture<GridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be greater than', () => {
    component.plants = {Content: [{plantName: 'Test'}]};
    component.ngOnChanges({
      plants:  new SimpleChange(null, component.plants, false) 
    });
    fixture.detectChanges();
    expect(component.gridData.length).toBeGreaterThan(0);
  });
});
