import { Component, OnInit, Input, ViewChild } from '@angular/core';

@Component({
  selector: 'grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit {
  gridData: any[];
  @Input() plants: any;

  constructor() {
    this.gridData = [];
  }
  
  ngOnInit(): void {
  }

  ngOnChanges(changes: any): void {
    if ('plants' in changes && this.plants) {
      this.setTableData();
    }
  }
  
  /**
   * map table data
   * @private
   */
  private setTableData(): void {
    this.gridData = [];
    this.plants.Content.forEach((row: any) => {
      this.gridData.push({
        plantName: row.Plant_name,
        absoluteValue: row.Plant_annual_net_generation_MWh,
        utilityName: row.Utility_name,
        percentage: (row.Plant_annual_net_generation_MWh) ? ((parseFloat(row.Plant_annual_net_generation_MWh) / this.plants.total_sum) * 100).toFixed(4) : 0
      });
    });
  }

}
