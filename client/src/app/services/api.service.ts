import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment }from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getStates() {
    return this.http.get(`${environment.apiUrl}/getStates`);
  }

  getTopNPlants(count: number) {
    return this.http.get(`${environment.apiUrl}/getTopNPlants/${count}`);
  }

  getTopNStatePlants(count: number, state: string) {
    return this.http.get(`${environment.apiUrl}/getTopNStatePlants/${state}/${count}`);
  }
}
